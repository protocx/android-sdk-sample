//
//  ViewController.swift
//  ProtoWebchat
//
//  Created by Tuan on 4/7/21.
//  Copyright © 2021 Proto. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler {
    
    let iOSInterface = "iOSInterface"
    
    lazy var webView: WKWebView = {
        let wkConfiguration = WKWebViewConfiguration()
        wkConfiguration.userContentController.add(self, name: iOSInterface)
        let wv = WKWebView(frame: .zero, configuration: wkConfiguration)
        wv.uiDelegate = self
        wv.navigationDelegate = self
        wv.translatesAutoresizingMaskIntoConstraints = false
        return wv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(webView)
        NSLayoutConstraint.activate([
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView.topAnchor.constraint(equalTo: view.topAnchor),
            webView.rightAnchor.constraint(equalTo: view.rightAnchor),
            webView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
        
        let url = Bundle.main.url(forResource: "index", withExtension: "html", subdirectory: "assets")!
        webView.loadFileURL(url, allowingReadAccessTo: url)
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        if message.name == iOSInterface {
            guard let dict = message.body as? [String: AnyObject],
                let param1 = dict["param1"] as? String,
                let param2 = dict["param2"] as? Int else {
                    return
            }
            showAlert(message: param1)
            
            // un-comment below line to send trigger to Proto when click on Close button
            //sendTriggerToProto(triggerApi: "show_toast")
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated {
            if navigationAction.request.url!.absoluteString == "proto://showtoast" {
                
                showAlert(message: "Trigger an action")
                //this tells the webview to cancel the request
                decisionHandler(.cancel)
                return
            }
            if navigationAction.request.url!.absoluteString == "proto://close" {
                
                // do stuff
                
                //this tells the webview to cancel the request
                decisionHandler(.cancel)
                return
            }
        }
        
        //this tells the webview to allow other requests
        decisionHandler(.allow)
    }
    
    private func showAlert(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        DispatchQueue.main.async {
            self.getTopMostViewController()?.present(alert, animated: true, completion: nil)
        }
    }
    
    private func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
        
        while let presentedViewController = topMostViewController?.presentedViewController {
            topMostViewController = presentedViewController
        }
        
        return topMostViewController
    }
    
    /// send trigger ,this is how we send data from iOS to Webview
    private func sendTriggerToProto(triggerApi: String) {
        let javascript = "Proto.sendTrigger('\(triggerApi)')"
        webView.evaluateJavaScript(javascript, completionHandler: nil)
    }

}

