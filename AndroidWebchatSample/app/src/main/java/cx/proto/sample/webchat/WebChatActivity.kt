package cx.proto.sample.webchat

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class WebChatActivity : AppCompatActivity() {

    private lateinit var myWebView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webchat)

        myWebView = findViewById(R.id.webview)

        myWebView.webChromeClient = WebChromeClient()
        myWebView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {

                if (url?.contains("proto://showtoast", true) == true) {
                    showToastAction("Trigger an action")
                }
                if (url?.contains("proto://close", true) == true) {
                    finish()
                }
                return true
            }

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {

                if (request?.url.toString().contains("proto://showtoast", true)) {
                    showToastAction("Trigger an action")
                }
                if (request?.url.toString().contains("proto://close", true)) {
                    finish()
                }
                return true
            }
        }

        myWebView.settings.javaScriptEnabled = true
        myWebView.settings.domStorageEnabled = true
        myWebView.addJavascriptInterface(WebAppInterface(this), "androidInterface")
        myWebView.loadUrl("file:///android_asset/index.html")

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.webview_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.trigger_button) {
            sendTriggerToProto("show_toast")
        }
        return super.onOptionsItemSelected(item)
    }

    private fun sendTriggerToProto(triggerApi: String?) {
        myWebView.loadUrl("javascript:Proto.sendTrigger('$triggerApi')")
    }

    private fun showToastAction(toast: String?) {
        Toast.makeText(this, toast, Toast.LENGTH_SHORT).show()
    }
}